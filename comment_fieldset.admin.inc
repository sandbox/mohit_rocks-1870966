<?php

/**
 * @file
 *  The file contains the administration settings for this module.
 */

function comment_fieldset_admin_settings() {
	$form['comment_fieldset_content_type'] = array(
			'#type' => 'fieldset',
			'#title' => t('Select content type for Comment Fieldset Wrapper'),
			'#description' => t('Select Yes for the content type on which you want to display comments in fieldset.'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);
	$node_types = _node_types_build()->names;
	foreach ($node_types as $node_type => $node_type_label) {
		$form['comment_fieldset_content_type']['comment_fieldset_content_type_value_' . $node_type] = array(
				'#type' => 'select',
				'#title' => $node_type_label,
				'#default_value' => variable_get('comment_fieldset_content_type_value_' . $node_type, 'yes'),
				'#options' => array('yes' => t('Yes'), 'no' => t('No')),
		);
	}
	return system_settings_form($form);
}